<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Blog extends Model
{
    protected $table = 'blog';

    protected $fillable = ['naslov', 'tekst', 'autor','titula'];


    public static function dohvatiSaId($id){
        return Blog::where('id', $id)->first();
    }


    public static function dohvatiSveAktivne(){
        return Blog::orderBy('created_at', 'desc')->get();
    }

   
}
