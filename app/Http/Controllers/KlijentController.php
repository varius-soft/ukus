<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Auth;
use Redirect;
use Hash;
use Session;
use Carbon\Carbon;
use Mail;
use App\Blog;
//use App\Traits\CaptchaTrait;
//use Illuminate\Support\Facades\Validator;

class KlijentController extends Controller
{
    public function blog()
    {
        $blog= Blog::dohvatiSveAktivne();

        //dd($blog);
        return view('blog',compact('blog'));
    }


    public function clanak($id)
    {
        $clanak=Blog::dohvatiSaId($id);
        $najnoviji=Blog::dohvatiSveAktivne();

    
        return view('clanak',compact('clanak','najnoviji'));
    }

    public function kontaktiraj(Request $request)
    {
        /*
        $request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);
*/
        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme

        ];


       //dd($data);
        //-----------------slanje maila-----------
        
         Mail::send('mailovi.kontakt', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('info@volimukus.rs', 'Poruka sa sajta- '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });
	
        
         Mail::send('mailovi.kontakt', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('smiljanic.95@gmail.com', 'Poruka sa sajta- '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });

        


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));

     return redirect('/uspesan-kontakt');
    }


    

    public function akcija_porucivanje(Request $request)
    {

        
        /*
        $request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);
*/
        $ime_prezime = $_POST['ime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $namirnice = $_POST['namirnice'];
        $adresa = $_POST['adresa'];
        $vreme = Carbon::now();
        
    
        //$stan = $_POST['stan'];
        $opcija = $_POST['dana'];
        $namirnice = $_POST['namirnice'];
        
        $poruka = " ";

        $on = $_POST['on'];
        $ona = $_POST['ona'];

        $jelovnik  = "MUŠKI: ". $on. "  +   ŽENSKI: " .$ona;

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'mail' => $mail,
           'vreme' => $vreme,
            'opcija' => $opcija,
            'namirnice'=> $namirnice,
            'jelovnik' => $jelovnik,
            'adresa' =>$adresa,
            'poruka' =>  $poruka,

           //dodajem namirnice i paket koji se porucuje

        ];


        //-----------------slanje maila-----------
        
        
         Mail::send('mailovi.poruci', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('info@volimukus.rs', 'PORUDZBINA - '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });

        
         Mail::send('mailovi.poruci', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('smiljanic.95@gmail.com', 'PORUDZBINA - '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });

        


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));

     return redirect('/uspesna-porudzbina');
    }

    public function porucivanje(Request $request)
    {

        
        /*
        $request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);
*/
        $ime_prezime = $_POST['ime_prezime'];
        $telefon = $_POST['telefon'];
        $mail = $_POST['mail'];
        $poruka = $_POST['poruka'];
        $adresa = $_POST['adresa'];
        $vreme = Carbon::now();
        
    
        //$stan = $_POST['stan'];
        $opcija = $_POST['opcija'];
        $namirnice = $_POST['namirnice'];
        $jelovnik = $_POST['jelovnik'];

        $vreme->tz='Europe/Belgrade';


         $data =[
           'ime_prezime' => $ime_prezime,
           'telefon' =>  $telefon,
           'poruka' => $poruka,
           'mail' => $mail,
           'vreme' => $vreme,
           //'stan' => $stan,
            'opcija' => $opcija,
            'namirnice'=> $namirnice,
            'jelovnik' => $jelovnik,
            'adresa' =>$adresa,

           //dodajem namirnice i paket koji se porucuje

        ];


       //dd($data);
        //-----------------slanje maila-----------
        
        
         Mail::send('mailovi.poruci', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('info@volimukus.rs', 'PORUDZBINA - '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });
    
        
         Mail::send('mailovi.poruci', $data, function($message) use ($ime_prezime,$mail) {
         $message->to('smiljanic.95@gmail.com', 'PORUDZBINA - '.$ime_prezime)->subject('Poruka sa sajta- '.$ime_prezime);
         $message->from('info@volimukus.rs' ,$ime_prezime.' - Poruka sa sajta');
        });

        


        //return view('mailovi.kontakt',compact('vreme','mail','poruka','telefon','ime_prezime'));

     return redirect('/uspesna-porudzbina');
    }

    
     public function kalkulator(Request $request)
    {
        /*
        $request['captcha'] = $this->captchaCheck();

        $request->validate([
            'ime_prezime' => 'required|string|max:254',
            'telefon' => 'required|string|max:19',
            'mail' => 'required|email|max:254',
            'g-recaptcha-response' => 'required',
            'captcha' => 'required|min:1',
        ]);
*/
        $aktivnost = $request->input('aktivnost'); 
        $cilj = $request->input('cilj');
        $namirnice = $request->input('namirnice');
        $pol = $request->input('pol');
        $tezina = $request->input('tezina');
        $visina = $request->input('visina');
        $dan = $request->input('dan');
        $vreme = Carbon::now();

        $vreme->tz='Europe/Belgrade';



        $jelovnik=0;
        $opis=0;
        $kalorije=0;
        $masti=0;
        $proteini=0;
        $hidrati=0;
        $cena_s=0;
        $cena_m=0;




        if($pol=="zenski")
        {
            switch ($cilj) {
                case 'mrsavljenje':
                    $jelovnik = "ŽENSKI REDUKCIONI";
                    $opis="Program ishrane sa kalorijskim deficitom u cilju redukcije telesne težine. Mršavite, ali ne gladujte, i uživajte u ukusu naših jela. Jelovnik je namenjen svim osobama koje žele redukovati potkožne i viscelarne masti uz očuvanje i unapređenje zdravstvenog statusa.";
                    $kalorije=1500;
                    $masti=25;
                    $proteini=25;
                    $hidrati=50;
                    $cena_s=1900;
                    $cena_m=1615;
                    break;

                case 'balans':
                    $jelovnik = "ŽENSKI BALANS";
                    $opis="Optimalno izbalansiran režim ishrane uz pomoć kojeg ćete održavati idealnu telesnu masu i snabdeti organizam svim neophodnim hranljivim materijama za pravilno funkcionisanje organizma, prevenciju bolesti i poboljšati produktivnost i kvalitet života";
                    $kalorije=1900;
                    $masti=25;
                    $proteini=25;
                    $hidrati=50;
                    $cena_s=2000;
                    $cena_m=1700;
                    break;

                case 'masa':
                    $jelovnik = "ŽENSKI MUSCLE";
                    $opis="Jelovnik je optimizovan u skladu sa povećanim potrebama osobe, idealan odnos proteina, masti i ugljenih hidrata. Namenjen je svim osobama koje vežbaju ili vode aktivan način života. Povećajte mišićnu masu, ali ne i potkožne masti.";
                    $kalorije=1900;
                    $masti=30;
                    $proteini=30;
                    $hidrati=40;
                    $cena_s=2100;
                    $cena_m=1785;
                    break;  

                default:
                    # code...
                    break;
            }
        }
        else
        {
            switch ($cilj) {
                case 'mrsavljenje':
                    $jelovnik = "MUŠKI REDUKCIONI";
                    $opis= "Program ishrane sa kalorijskim deficitom u cilju redukcije telesne težine. Mršavite, ali ne gladujte, i uživajte u ukusu naših jela. Jelovnik je namenjen svim osobama koje žele redukovati potkožne i viscelarne masti uz očuvanje i unapređenje zdravstvenog statusa.";
                    $kalorije=1900;
                    $masti=25;
                    $proteini=25;
                    $hidrati=50;
                    $cena_s=2000;
                    $cena_m=1700;
                    break;

                case 'balans':
                    $jelovnik = "MUŠKI BALANS";
                    $opis="Optimalno izbalansiran režim ishrane uz pomoć kojeg ćete održavati idealnu telesnu masu i snabdeti organizam svim neophodnim hranljivim materijama za pravilno funkcionisanje organizma, prevenciju bolesti i poboljšati produktivnost i kvalitet života";
                    $kalorije=2500;
                    $masti=25;
                    $proteini=25;
                    $hidrati=50;
                    $cena_s=2100;
                    $cena_m=1785;
                    break;

                case 'masa':
                    $jelovnik = "MUŠKI MUSCLE";
                    $opis="Jelovnik je optimizovan u skladu sa povećanim potrebama osobe, idealan odnos proteina, masti i ugljenih hidrata. Namenjen je svim osobama koje vežbaju ili vode aktivan način života. Povećajte mišićnu masu, ali ne i potkožne masti.";
                    $kalorije=2500;
                    $masti=30;
                    $proteini=30;
                    $hidrati=40;
                    $cena_s=2200;
                    $cena_m=1870;
                    break;  

                default:
                    # code...
                    break;
            }
        }



        //dd($data);
     return view('rezultat',compact('aktivnost','cilj','namirnice','pol', 'tezina','visina','vreme','dan' ,'jelovnik','opis','kalorije','masti','proteini','hidrati','cena_s','cena_m'));
    }
}