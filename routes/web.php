<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/blog','KlijentController@blog');

Route::get('/clanak/{id}','KlijentController@clanak');

Route::get('/kontakt', function () {
    return view('kontakt');
});

/*
Route::get('/poruci', function () {
    return view('poruci');
});

*/
Route::get('/cenovnik', function () {
    return view('cenovnik');
});


Route::post('/poruci-paket', 'KlijentController@porucivanje');


Route::post('/kontaktiraj', 'KlijentController@kontaktiraj');

Route::post('/akcija-porucivanje', 'KlijentController@akcija_porucivanje');

Route::get('/kalkulator-rezultat', 'KlijentController@kalkulator');


Route::get('/poruci', function () {
    return view('poruci');
});


Route::get('/sta-je-ukus', function () {
    return view('sta_je_ukus');
});


Route::get('/ambasadori', function () {
    return view('ambasadori');
});


Route::get('/cesta-pitanja', function () {
    return view('pitanja');
});

Route::get('/jelovnik', function () {
    return view('jelovnik');
});


Route::get('/ukus-office', function () {
    return view('office');
});
   
Route::get('/beskontaktno-placanje-dostava', function () {
    return view('beskontaktno');
});

Route::get('/uspesan-kontakt', function () {
    return view('uspesan_kontakt');
});


Route::get('/uspesna-porudzbina', function () {
    return view('uspesna_porudzbina');
});
                     

//----------------paketi pocetak--------------
Route::get('/topi-kilograme', function () {
    return view('jelovnici.topi');
});

Route::get('/detox', function () {
    return view('jelovnici.detox');
});

Route::get('/gradi-misice', function () {
    return view('jelovnici.gradi');
});


Route::get('/zivi-zdravo', function () {
    return view('jelovnici.zdravo');
});




//----------------paketi kraj--------------