@extends('layout')

@section('title')
Poruči - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Poruči UKUS</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Poruči UKUS</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->



<!-- Contacts -->
<section class="boxes" id="contacts">
    <div class="container-fluid">
        
        <div class="row">
            
            <!-- Contacts -->
            <div class="col-md-6 bordered_block image_bck white_txt" data-image="images/pozadina-crna.jpg">
                <div class="over" data-opacity="0.7" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <h2>Poručite Vaš personalizovani jelovnik!</h2>
                    <p>
                        Unesite potrebne parametre i mi ćemo za Vas napraviti personalizovani jelovnik koji savršeno odgovara Vašim svakodnevnim aktivnostima, načinu života, kao i cilju koji želite da postignete.
                    </p>
                    
                    <img style="height: 160px;" src="images/almond_PNG11.png">
                    <img style="height: 160px;" src="images/personalizovani-jelovnik.png">
                    
                </div>    
            </div>
            
            <!-- Write Us -->
           <div class="col-md-6 bordered_block image_bck grey_border" data-color="#fff">
                <div class="over" data-opacity="0.02" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                     <form action="/kalkulator-rezultat" name="CALC" id="CALC">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="visina" class="form-control form-opacity" placeholder="Visina (u cm)*">
                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="tezina" class="form-control form-opacity" placeholder="Težina (u kg)*">
                            </div>
                             
                            <div class="col-md-12">
                               <select required name="pol" class="form-control">
                                    <option selected disabled="">Pol</option>
                                    <option value="zenski">Ženski</option>
                                    <option value="muski">Muški</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="cilj" class="form-control">
                                    <option selected disabled="">Želim da:</option>
                                    <option value="mrsavljenje">Izgubim višak kilograma</option>
                                    <option value="balans">Da se hranim izbalansirano</option>
                                    <option value="masa">Povećam mišićnu masu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="aktivnost" class="form-control">
                                    <option selected disabled="">Nivo fizičke aktivnosti:</option>
                                    <option value="0">Ne treniram uopšte</option>
                                    <option value="1">1-2 treninga nedeljno</option>
                                    <option value="2">3-4 treninga nedeljno</option>
                                    <option value="3">5-7 treninga nedeljno</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="dan" class="form-control">
                                    <option selected disabled="">Koliko ste aktivni tokom dana?</option>
                                    <option value="0">Uglavnom sedim (automobil/kancelarija)</option>
                                    <option value="1">Umereno sam aktivan/na</option>
                                    <option value="2">Stalno sam u pokretu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Navedite namirnice koje ne želite da se nađu na Vašem personalizovanom jelovniku (ili ste alergični na neke od namirnica)."  class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12  text-center">
                                <input type="submit" form="CALC" class="submit btn btn-default btn-lg active" value="Prikaži rezultat">
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- Write Us End -->

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- Contacts End -->



<!-- Partners -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/furn6.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                <div class="container text-center">

                <h2>UKUS U VAŠOJ FIRMI? POGLEDAJTE UKUS OFFICE</h2>
                <p>UKUS Office je koncept zdrave ishrane namenjen pravnim licima. Umesto svih obroka, Vašim zaposlenima svakog radnog dana isporučujemo jedan obrok i jednu užinu na adresu fimre.<BR><BR></p>
                <a class="btn btn-default" href="/ukus-office">DETALJNIJE</a>
                  
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--Partners End -->
@stop