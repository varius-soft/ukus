<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>@yield('title') UKUS</title>

<!-- Bootstrap CSS -->
<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">

<!-- Font Awesome CSS -->
<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{asset('fonts/themify-icons.css')}}" rel="stylesheet">

<!-- Owl Carousel CSS -->
<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">

<!-- Magnific Popup core CSS file -->
<link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}"> 

<!-- Animate CSS -->
<link rel="stylesheet" href="{{asset('css/animate.css')}}"> 


<!-- Theme CSS -->
<link href="{{asset('css/style.css')}}" rel="stylesheet">

<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Seaweed+Script' rel='stylesheet' type='text/css'>

@yield('meta')
    
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-117964354-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-117964354-11');
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '451532445713862');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=451532445713862&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



<style >
        
        .header-top-black {
              position: fixed; 
              top: 0; 
              width: 100%; 
              padding-top: 6px;
              
              background: black;
              color: white;
              text-align: center;
            }
    
        .telefon {
            display:none;
        }

        .komp{
            display:block;
        }
     
        @media only screen and (min-device-width:600px){
            
            .telefon {
            display:block;
            }
            
            .komp{
            display:none;
        }

        }
        
        <style>
.mySlides {display:none;}
</style>

    </style>
</head>
<body class=""> <!--gold_title font-Seaweed-Script rounded-->

<!-- Passpartu -->
<div class="passpartu passpartu_left"></div>
<div class="passpartu passpartu_right"></div>
<div class="passpartu passpartu_top"></div>
<div class="passpartu passpartu_bottom"></div>
<!-- Passpartu End -->

<div class="page" id="page">

<!-- Header white_bck black_bck -->
<header class="white_bck" >


   <div class="top_line clearfix" style="color: white;">

        <span class="tl_item">
            <a href="mailto:info@volimukus.rs">
                <img src="/images/mail.png" height="25px;" > 
            </a>
        </span>

        <span class="tl_item">
            <a style="color: white;" href="https://www.instagram.com/volim_ukus/" rel="nofollow" target="_blank">
                <img src="/images/insta.png" height="25px;" > 
            </a>
        </span>

        <span class="tl_item">
            <a style="color: white;" href="https://www.facebook.com/Volim-Ukus-101787471253649/?view_public_for=101787471253649" rel="nofollow" target="_blank">
                <img src="/images/fb.png" height="25px;" > 
            </a>
        </span>

         
   
        <div class="pull-right">
            <span class="tl_item">
                <!--
                <span class="ti ti-mobile"></span>
            --><a style="color: black;" href="tel:069 8 10 20 30"><STRONG>PORUČI: 069 8 10 20 30 </STRONG></a>
            </span>
        </div>
              
    </div>

    <!-- Logo -->
    <div class="logo pull-left">
        <a href="/"><img style="height: 50px;" src="{{asset('images/logo.png')}}"></a>
    </div>


    <!-- Header Buttons -->
    <div class="header_btns_wrapper ">

        <!-- Main Menu Btn -->
        <div class="main_menu"><i class="ti-menu"></i><i class="ti-close"></i></div>
        
        <!-- Sub Menu -->
        <div class="sub_menu">
            <div class="sub_cont">
                <ul>
                    <!--
                    <li>
                        <a href="/">Naslovna</a>
                    </li>
                    -->
                    <li><a href="/sta-je-ukus">Šta je ukus?</a>

                    </li>

                    <li><a href="#" class="parents">UKUS Programi</a>
                        <ul class="mega_menu">
                            <li class="mega_sub">
                                <ul>
                                    <li><a href="/topi-kilograme">Topi Kilograme</a></li>
                                    <li><a href="/detox">Detox</a></li>
                                    <li><a href="/gradi-misice">Gradi Mišiće</a></li>
                                </ul>
                            </li>
                            
                        </ul>
                    </li>

                    <li>
                        <a href="/ukus-office">UKUS Office</a>
                    </li>
                    
                    <li>
                        <a href="/zivi-zdravo">Živi zdravo</a>
                       
                    </li>
                
                    
                    <li>
                        <a href="/cenovnik">Poruči ukus</a>
                    </li>

                    <li>
                        <a href="/ambasadori">UKUS AMBASADORI</a>                          
                    </li>
                    <li>
                        <a href="/blog">Blog</a>
                    </li>
                    <li>
                        <a href="/kontakt">Kontakt</a>                          
                    </li>

        

                   
                </ul>


            </div>
        </div>
        <!-- Sub Menu End -->

    </div>
    <!-- Header Buttons End -->
    
    <!-- Up Arrow -->
    <a href="#page" class="up_block go"><i class="fa fa-angle-up"></i></a>

</header>
<!-- Header End -->
    

@yield('sekcije')

<!-- Footer -->
<div class="footer white_txt bordered_wht_border image_bck" style="background-color: rgb(14, 14, 14);" data-color="#0e0e0e" data-position="bottom">
    <div class="container">
        
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <div class="widget">
                    <a href="/"><img style="width: 90%;" src="{{asset('images/logo-beli.png')}}"></a>
                </div>
            </div>
            
            <div class="col-md-3 col-sm-12 ">
                <div class="widget">
                    <h4>Šta je UKUS?</h4>
                    <ul class="list-unstyled">
                        <li><a href="/sta-je-ukus">- O Ukusu</a></li>
                        <li><a href="/sta-je-ukus#kako-smo-nastali">- Zašto smo nastali?</a></li>
                </div>

            </div>
            
            <div class="col-md-3 col-sm-12 ">
                <div class="widget">
                    <h4>Probaj UKUS!</h4>
                    <ul class="list-unstyled">
                        <li><a href="/poruci">- Poruči UKUS</a></li>
                        
                        <li><a href="/cenovnik">- Cenovnik</a></li>
                    
                        <li><a href="/ukus-office">- UKUS Office</a></li>
                    </ul>
                </div>
                <!--end of widget-->
            </div>
            
            <div class="col-md-3 col-sm-12">
                <div class="widget">
                    <h4>Kontaktirajte nas</h4>

                    <span class="contacts_ti ti-mobile"></span><a style="color: white;" href="tel:069 8 10 20 30">069 8 10 20 30</a><br />
                    <span class="contacts_ti ti-email"></span><a style="color: white;" href="mailto:info@volimukus.rs">info@volimukus.rs</a><br />
                    <span class="contacts_ti ti-location-pin"></span>BEOGRAD , Srbija
                </div>
                <!--end of widget-->
            </div>
        
        </div>
        <!--Row End-->
   
    </div>
    <!-- Container End -->

    <!-- Footer Copyrights -->
    <div class="footer_end">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <span class="sub">&copy; Sva prava zadržana. Volim Ukus</span>
                </div>
                <div class="col-sm-6 text-right">
                    <ul class="list-inline social-list">
                        
                        <li>
                            <a href="https://www.facebook.com/Volim-Ukus-101787471253649/?view_public_for=101787471253649"  data-toggle="tooltip" data-placement="top" rel="nofollow" target="_blank" title="Facebook">
                                <i class="ti-facebook"></i>
                            </a>

                        </li>
                        <li>
                            <a href="https://www.instagram.com/volim_ukus/" data-toggle="tooltip" data-placement="top" title="Instagram" rel="nofollow" target="_blank">
                                <i class="ti-instagram"></i>
                            </a>
                        </li>
                        
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyrights End -->


</div>
<!-- Footer End -->


</div>
<!-- Page End -->

@yield('scriptbottom')

<!-- JQuery -->
<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script> 
<!-- WL Carousel JS -->
<script src="{{asset('js/owl.carousel.min.js')}}"></script>

<!-- PrefixFree -->
<script src="{{asset('js/prefixfree.min.js')}}"></script>
<!-- Magnific Popup core JS file -->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<!-- Textillate -->
<script src="{{asset('js/jquery.lettering.js')}}"></script>
<!-- Countdown -->
<script src="{{asset('js/jquery.plugin.min.js')}}"></script> 
<script src="{{asset('js/jquery.countdown.min.js')}}"></script>
<!-- JQuery UI -->
<script src="{{asset('js/jquery-ui.js')}}"></script>
<!-- Wow -->
<script src="{{asset('js/wow.js')}}"></script>
<!-- Masonry -->
<script src="{{asset('js/masonry.pkgd.min.js')}}"></script>

<!-- Bootstrap JS -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Theme JS -->
<script src="{{asset('js/script.js')}}"></script>

<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>

<!--
<script language="JavaScript">

  window.onload = function() {
    document.addEventListener("contextmenu", function(e){
      e.preventDefault();
    }, false);
    document.addEventListener("keydown", function(e) {
    //document.onkeydown = function(e) {
      // "I" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
        disabledEvent(e);
      }
      // "J" key
      if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
        disabledEvent(e);
      }
      // "S" key + macOS
      if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
        disabledEvent(e);
      }
      // "U" key
      if (e.ctrlKey && e.keyCode == 85) {
        disabledEvent(e);
      }
      // "F12" key
      if (event.keyCode == 123) {
        disabledEvent(e);
      }
    }, false);
    function disabledEvent(e){
      if (e.stopPropagation){
        e.stopPropagation();
      } else if (window.event){
        window.event.cancelBubble = true;
      }
      e.preventDefault();
      return false;
    }
  };

</script>
-->
</body>

</html>