@extends('layout')

@section('title')
KONTAKT USPEŠAN - 
@stop

@section('sekcije')

<!--Intro-->
<section class="intro" >
       
        <div class="intro_item">

            <!-- Over -->
            <div class="over" data-opacity="0.4" data-color="#000"></div>
            <div class="into_back image_bck"  data-image="images/sport7.jpg"></div>
            

            <div class="inside_intro_block">
                <div class="ins_int_item white_txt bordered_wht_border text-center">
                    <div class="simple_block simple_block_sml">
                    	<div class="icon"><span class="ti-email"></span></div>
                        <h3>USPEŠNO STE NAS KONTAKTIRALI</h3>
                        <h4>
                            Potrudićemo se da Vam odgovorimo u najkraćem mogućem roku.<br>Hvala.
                        </h4>
                        <a href="/" class="btn btn-white"><i class="ti-angle-left"></i> Nazad na početnu stranu</a> 
                       
                    </div>
                </div>
            </div>

        </div>
  
</section>
<!-- Intro End -->
    

@stop