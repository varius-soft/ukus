@extends('layout')

@section('title')
Najčešća pitanja - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Najčešća pitanja i odgovori</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Najčešća pitanja i odgovori</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<div class="row">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container">
        	<h2></h2>
        	<h3>Ovde možete pronaći odgovore na sva najčešće postavljana pitanja našem timu. Ukoliko ne pronađete sve potrebne informacije  <a href="/kontakt">kontaktirajte nas.</a>
        	 </h3>
        </div>
    </div>
</div> 




<!-- Content -->
<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="bordered_block col-md-12 grey_border">
                
                <div class="container">
                    <div class="row">
                    
                        <!--Sidebar-->
                        <div class="col-md-12 col-xs-12">
                            
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      Šta ako sam alergičan na neku od namirnica?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                  <div class="panel-body">
                                    Ukoliko ste alergični na neku od namirnica ili jednostavno ne želite da se nađe na jelovniku iz bilo kog razloga, dovoljno je samo da ih navedete u našem <a href="/poruci">KALKULATORU</a>.
                                  </div>
                                </div>
                              </div>
                              
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                     	U koliko sati se vrši isporuka obroka za taj dan?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Isporuka je svakodnevna i obavlja se od 11 do 14 časova na željenoj lokaciji.
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseThree">
                                     	Da li je moguće probati UKUS pre poručivanja?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Ukoliko niste sigurni da li je UKUS pravo rešenje za Vas, savetujemo Vam da poručite dnevni meni i probate ukus UKUSa. 
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseThree">
                                     	Da li organizujete degustacije u firmama?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Ukoliko je Vaša firma zainteresovana za saradnju sa nama, kontaktirajte nas putem <a href="/kontakt"> kontakt forme.</a> Vrlo rado ćemo Vam izaći u susret i orgranizovati degustaciju.
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree">
                                     	Da li imate veganski jelovnik?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Veganski jelovnik je u pripremi i bićete blagovremeno obavešteni na sajtu.
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                  <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapseThree">
                                     	Da li je moguća dostava na različite adrese u zavisnosti od dana u nedelji?
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                  <div class="panel-body">
                                    Naravno, dostava na više adresa je moguća na više lokacija, u zavisnosti od Vaših potreba i svakodnevnih aktivnosti. Za promenu lokacije kontaktirajte nas putem e-maila : <a href="mailto:info@volimukus.rs">info@volimukus.rs</a>.
                                  </div>
                                </div>
                              </div>
                            </div>

                            
                        </div>
                        <!--Sidebar End-->
                        
                    </div>
                    <!--Row End-->

                </div>
            </div>
        </div> 
        <!-- Row End -->


    </div>
</div>
<!-- Content End -->


@stop