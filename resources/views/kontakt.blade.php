@extends('layout')

@section('title')
Kontakt - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Kontakt</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Kontakt</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<div class="row">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container">
        	<h2></h2>
        	<h3>Kontaktirajte nas za sve potrebne informacije ili poručivanje personalizovanog jelovnika. <br>
        	Najčešća pitanja i odgovore možete pogledati <a href="/cesta-pitanja">ovde.</a>
        	 </h3>
        </div>
    </div>
</div> 


<!-- Contacts -->
<section class="boxes" id="contacts">
    <div class="container-fluid">
        
        <div class="row">
            
            <!-- Contacts -->
            <div class="col-md-6 bordered_block image_bck white_txt" data-image="images/pozadina-crna.jpg">
                <div class="over" data-opacity="0.7" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <h2>Informacije</h2>
                    <span class="contacts_ti ti-mobile"></span><a href="tel:069 8 10 20 30">069 8 10 20 30</a><br />
                    <span class="contacts_ti ti-email"></span><a href="mailto:info@volimukus.rs">info@volimukus.rs</a><br />
                    <span class="contacts_ti ti-location-pin"></span>11000, Beograd
                    
                </div>    
            </div>
            
            <!-- Write Us -->
           <div class="col-md-6 bordered_block image_bck grey_border" data-color="#fff">
                <div class="over" data-opacity="0.02" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <h2>Kontaktirajte nas</h2>
                    <form action="/kontaktiraj" id="kontakt_forma" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <input style="color: black;" type="text" id="ime_prezime" name="ime_prezime" class="form-control form-opacity" placeholder="Ime*">
                            </div>
                            
                            
                            <div class="col-md-6">
                                <input style="color: black;" type="text" name="mail" id="email" class="form-control form-opacity" placeholder="E-mail*">
                            </div>
                            <div class="col-md-6">
                                <input style="color: black;" name="telefon" type="text" id="phone" class="form-control form-opacity" placeholder="Telefon">
                            </div>
                            <div class="col-md-12">
                                <textarea style="color: black;" name="poruka" placeholder="Poruka" id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12  text-center">
                                <input type="submit" form="kontakt_forma" class="submit btn btn-default btn-lg active" value="Pošalji">
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- Write Us End -->

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- Contacts End -->

@stop