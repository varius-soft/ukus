@extends('layout')

@section('title')
Cenovnik - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Cenovnik</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Cenovnik</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

 <div class="row">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container">
            <div class="" data-example-id="striped-table">
            <table class="table table-striped" style="color: #464646;">
              <thead>
                <tr>
                  <th><strong>ŽENSKI PAKETI</strong></th>
                  <th>1 DAN</th>
                  <th>5/6 DANA</th>
                  <th>20/24 DANA</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">TOPI KILOGRAME</th>
                  <td> 2,400 </td>
                  <td> 1,900 </td>
                  <td> 1,615 </td>
                  <td> <a href="/topi-kilograme"> PORUČI</a> </td>
                </tr>
                <tr>
                  <th scope="row">ŽIVI ZDRAVO</th>
                  <td> 2,400 </td>
                  <td> 2,000 </td>
                  <td> 1,700 </td>
                  <td> <a href="/zivi-zdravo"> PORUČI</a> </td>
                </tr>
                <tr>
                  <th scope="row">GRADI MIŠIĆE</th>
                  <td> 2,400 </td>
                  <td> 2,100 </td>
                  <td> 1,785 </td>
                  <td> <a href="/gradi-misice"> PORUČI</a> </td>
                </tr>
              </tbody>
            </table>
          </div><!-- /example -->

          <div style="padding-top: 30px;" class="" data-example-id="striped-table">
            <table class="table table-striped" style="color: #464646;">
              <thead>
                <tr>
                  <th><strong>MUŠKI PAKETI</strong></th>
                  <th>1 DAN</th>
                  <th>5/6 DANA</th>
                  <th>20/24 DANA</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">TOPI KILOGRAME</th>
                  <td> 2,400 </td>
                  <td> 2,000 </td>
                  <td> 1,700 </td>
                  <td> <a href="/topi-kilograme"> PORUČI</a> </td>
                </tr>
                <tr>
                  <th scope="row">ŽIVI ZDRAVO</th>
                  <td> 2,400 </td>
                  <td> 2,100 </td>
                  <td> 1,785 </td>
                  <td> <a href="/zivi-zdravo"> PORUČI</a> </td>
                </tr>
                <tr>
                  <th scope="row">GRADI MIŠIĆE</th>
                  <td> 2,400 </td>
                  <td> 2,200 </td>
                  <td> 1,870 </td>
                  <td> <a href="/gradi-misice"> PORUČI</a> </td>
                </tr>
              </tbody>
            </table>
          </div><!-- /example -->



            <p>
               <strong>CENA DETOX PAKETA JE 12 000 RSD NA NEDELJNOM NIVOU.  <a href="/detox"> PORUČI DETOX PAKET.</a> </strong><BR><BR><BR>

            	*Cene su izražene u dinarima po danu, po osobi.
            </p>
        </div>
    </div>
</div> 


<!-- Partners -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/furn6.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                <div class="container text-center">

                <h2>UKUS U VAŠOJ FIRMI? POGLEDAJTE UKUS OFFICE</h2>
                <p>UKUS Office je koncept zdrave ishrane namenjen pravnim licima. Umesto svih obroka, Vašim zaposlenima svakog radnog dana isporučujemo jedan obrok i jednu užinu na adresu fimre.<BR><BR></p>
                <a class="btn btn-default" href="/ukus-office">DETALJNIJE</a>
                  
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--Partners End -->



@stop