@extends('layout')

@section('meta')
<meta property="og:title" content="Volim UKUS" />
<meta property="og:description" content="UKUS je koncept zdravih, svežih namirnica, bez aditiva, zaslađivača i konzervanasa.
UKUS sadrži izbalansirane obroke u odnosu ugljenih hidrata, masti i proteina." />
<meta property="og:image" content="http://volimukus.rs/images/logo-beli.png" />


<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

@stop

@section('sekcije')
<!--Hotel-->
<!--
<img src="images/pokazivanje/33.jpg" style="width: 100%;">
<section class="intro" style="padding-top: 100px; ">


    <a href="#welcome" class="down_block go"><i class="fa fa-angle-down"></i></a>


    <div class="intro_wrapper">

        
        <div class="intro_item">

     
            <div class="over" data-opacity="0.0" data-color="#292929"></div>
            <div class="into_back into_zoom image_bck" data-image="images/pokazivanje/marko1.jpg"></div>
        </div>

        
       <div class="intro_item">

         
            <div class="over" data-opacity="0.0" data-color="#292929"></div>
            <div class="into_back into_zoom image_bck" data-image="images//novo/ukus-baner-2.jpg"></div>
        </div>

        <div class="intro_item">

      
            <div class="over" data-opacity="0.0" data-color="#292929"></div>
            <div class="into_back into_zoom image_bck" data-image="images//novo/ukus-baner-3.jpg"></div>
        </div>

        

    </div>

    
</section>
-->

<!-- Intro End -->
<!-- Gallery -->
<!-- Slideshow container -->
<div class="" style="width: 100%; padding-top: 60px;">
  <img class="mySlides" src="images/novo/ukus-baner-2.jpg" style="width:100%;">
  <img class="mySlides" src="images/novo/ukus-baner-3.jpg" style="width:100%">
  <img class="mySlides" src="images/novo/ukus-baner-1.jpg" style="width:100%">
  <img class="mySlides" src="images/novo/ukus-baner-3.jpg" style="width:100%">

  <button class="w3-button w3-black w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
  <button class="w3-button w3-black w3-display-right" onclick="plusDivs(1)">&#10095;</button>
</div>
<div class="after_slider_border"></div>

<!--
<a href="/beskontaktno-placanje-dostava">
<img class="telefon" src="/images/akcija-dan-zaljubljenih-1.jpg" style="width: 100%; padding-top: 10px;">  
<img class="komp" src="/images/beskontaktna-dostava-placanje.jpg" style="width: 100%; padding-top: 70px;">  
</a>

-->



<img src="/images/pokazivanje/234.jpg" style="width: 100%; padding-top: 10px;">  

<DIV>
<img class="telefon" src="/images/novo/ukus-traka-1.jpg" style="width: 100%">
<img class="komp" src="/images/novo/ukus-traka-1.jpg" style="width: 100%"> 
</DIV>



<DIV CLASS="container">
    JELOVNICI
</DIV>




<!-- UKUS OFFICE -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/novo/ukus-pozadina-2.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.5" data-color="#292929"></div>
                <div class="container text-center">

                <!--<h1>UKUS OFFICE</h1>-->
                <h2>Na poslu ste i dosta Vam je fast food-a? Želite da pojedete nešto zdravo?

                </h2>

                <H1>
                    <STRONG>UKUS OFFICE JE PRAVO REŠENJE ZA VAS!</STRONG>
                </H1>
                <p>Podignite produktivnost i lakoću rada uz UKUS OFFICE program. 
                
                    <br>
                    Ručak i užina na Vašoj adresi 5 dana u nedelji, svakodnevno do 14<sup>00</sup> h
                </p>
                <h1>CENA : 650 RSD</h1>

                    <a class="btn btn-default" href="/kontakt">PORUČI!</a>
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--UKUS OFFICE  End -->



<DIV>
   
<img class="telefon" src="/images/pokazivanje/567.jpg" style="width: 100%">
<img class="komp" src="/images/okazivanje/567.jpg" style="width: 100%">
</DIV>


<!-- UKUS DETOX -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/novo/ukus-pozadina-1.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                    <div class="container text-center">

                    <h1>DETOX</h1>
                    <h2>
                        7 dana potpuno izbalansirane vegeterijanske hrane većinski presne(sirove).
                    </h2>
                    <p>
                        <strong>
                            Očistite Vaš organizam od svih štetnihmaterija unetih hranom, alkoholom, duvanom i drugim štetnim namirnicama.
                        </strong>
    <BR>
                            DETOX program je idealan uvod u bilo koji od naših programa:<strong> TOPI KILOGRAME, GRADI MIŠIĆE, ŽIVI ZDRAVO!</strong>
                        
                    </p>
                        <h1>CENA NEDELJNOG PAKETA JE 12 000 RSD</h1>

                        <a class="btn btn-default" href="#">PORUČI!</a>
                    </div>
                
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--UKUS DETOX  End -->


<DIV CLASS="">
    <!--
    <h1 class="text-center">NARUČITE VAŠ DNEVNI PAKET PO PROMOTIVNOJ CENI</h1>
-->
<img class="telefon" src="/images/novo/ukus-baner-33.jpg" style="width: 100%">
<img class="komp" src="/images/novo/ukus-baner-33.jpg" style="width: 100%">
</DIV>


<!--Team-->
<section class="boxes reviews" style="padding-top: 10px;">
    <div class="container-fluid">

        
        <div class="mid_wrapper">
            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/I1.jpg"></div>
                  
            </div>
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/I2.jpg"></div>
                  
            </div>
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/I3.jpg"></div>
                  
            </div>
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/I4.jpg"></div>
                  
            </div>
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/I5.jpg"></div>
                  
            </div>


            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/milan.jpg"></div>
                <div class="box_content">
                    <h3>Milan Marić</h3>
                    
                </div>     
            </div>

            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/masan.jpg"></div>
                <div class="box_content">
                    <h3>Mašan Lekić</h3>
                   
                </div>     
            </div>

            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/ilda.jpg"></div>
                <div class="box_content">
                    <h3>Ilda Šaulić</h3>
                    
                </div>     
            </div>

            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/laza.jpg"></div>
                <div class="box_content">
                    <h3>Dušan Lazić Laza</h3>
                     
                </div>     
            </div>



            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/vaso.jpg"></div>
                <div class="box_content">
                    <h3>Vaso Bakočević</h3>
                    
                </div>     
            </div>

            
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/poznati/ivana.jpg"></div>
                <div class="box_content">
                    <h3>Ivana Kostić</h3>
                </div>     
            </div>

          

        </div>
        
    </div>
</section>
<!--Team End-->



@stop