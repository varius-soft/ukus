@extends('layout')

@section('title')
UKUS Ambasadori - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">UKUS Ambasadori</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>UKUS Ambasadori</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<img src="/images/ukus-baner-ambasadori.jpg" style="width: 100%;">

<!-- Partners -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/sport7.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                <div class="container text-center">

                <h2>TVOJE ZANIMANJE JE TRENER ILI NUTRICIONISTA I ŽELIŠ DA ZARADIŠ OD KUĆE?</h2>
                <p>
                    <strong>
                    Našem timu su potrebni novi članovi. Ukoliko je TVOJE zanimanje nutricionista ili trener javi nam se putem telefona <a href="tel:069 8 10 20 30 ">069 8 10 20 30 </a> ili e-mail <a href="mailto:info@volimukus.rs">info@volimukus.rs</a> kako bismo ti pružili sve potrebne informacije o potencijlnoj saradnji. 
                    </strong>
                </p>
                <h3><b>Postani </b> AMBASADOR UKUSa!<BR></h3>
                <a class="btn btn-default" href="mailto:info@volimukus.rs">
                    <span class="contacts_ti ti-email"></span> info@volimukus.rs 
                </a>

                <a class="btn btn-default" href="tel:069 8 10 20 30">
                    <span class="contacts_ti ti-mobile"></span> 069 8 10 20 30 

                </a>
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--Partners End -->

@stop



