@extends('layout')

@section('title')
AKCIJA - 
@stop

@section('sekcije')

<img style="padding-top: 20px;" src="images/akcija-dan-zaljubljenih-1.jpg" width="100%">



<!-- Contacts -->
<section class="boxes" id="contacts">
    <div class="container-fluid">
        
        <div class="row">
            
            <!-- Contacts -->
            <div class="col-md-6 bordered_block image_bck white_txt" data-image="images/pozadina-crna.jpg">
                <div class="over" data-opacity="0.7" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <h2 id="rezultat">AKCIJA - NEDELJA LJUBAVI U UKUSU!</h2>
                    <p>
                        <strong>
                            Poručite za Vas i Vama dragu osobu UKUS nedeljni meni i ostvarite popust od 30%* na jeftiniji paket. U akciji učestvuju svi nedeljni paketi (od 5 i 6 dana).

                        </strong>

                        <br><br>
                        *Popust se odnosi na jedan - jeftiniji paket. Akcija važi za prijave do 14.2.2020. Paketi iz akcije se dostavljaju na istu adresu.
                    </p>
                    
                    <h4 id="polje_ona">PAKET ZA NJU: </h4>
                    <h4 id="polje_on">PAKET ZA NJEGA: </h4>
                    <h4 id="usteda">UŠTEDA: </h4>
                    <h3 id="ukupno">UKUPNO </h3>
                </div>    
            </div>
            
            <!-- Write Us -->
           <div class="col-md-6 bordered_block image_bck grey_border" data-color="#fff">
                <div class="over" data-opacity="0.02" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                     <form action="/akcija-porucivanje" method="POST" name="CALC" id="CALC">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="ime" class="form-control form-opacity" placeholder="Ime i prezime">
                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="telefon" class="form-control form-opacity" placeholder="Telefon">
                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="mail" class="form-control form-opacity" placeholder="E-mail">
                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="adresa" class="form-control form-opacity" placeholder="Adresa">
                            </div>
                             
                            <div class="col-md-12">
                               <select required name="ona" id="ona" class="form-control" onchange="myFunction()">
                                    <option value="0" selected disabled="">Paket za NJU:</option>
                                    <option  value="redukcioni">Redukcioni</option>
                                    <option value="balans">Balans</option>
                                    <option value="muscle">Muscle</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="on" id="on"  class="form-control" onchange="myFunction()">
                                    <option value="0" selected disabled="">Paket za NJEGA:</option>
                                    <option value="redukcioni">Redukcioni</option>
                                    <option value="balans">Balans</option>
                                    <option value="muscle">Muscle</option>
                                </select>
                            </div>

                            <div class="col-md-12" >
                               <select required name="dana" id="dana" class="form-control" onchange="myFunction()">
                                    <option value="0"  disabled="">Broj dana:</option>
                                    <option value="5" selected>5 dana (ponedeljak - petak)</option>
                                    <option value="6">6 dana (ponedeljak - subota)</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Navedite namirnice koje ne želite da se nađu na Vašem personalizovanom jelovniku (ili ste alergični na neke od namirnica)."  class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12  text-center">
                                <input type="submit" form="CALC" class="submit btn btn-default btn-lg active" value="PORUČI">
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- Write Us End -->

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- Contacts End -->


@stop



@section('scriptbottom')

<script>
function myFunction() {
  var ona = document.getElementById("ona").value;
  var on = document.getElementById("on").value;
  var dana = document.getElementById("dana").value;

  

    var cena_ona = 0;
    var cena_on = 0;
    var snizenje = 0;
    var ukupno = 0;
    var usteda = 0;

  switch (ona) {
  case 0:
    cena_ona = 0;
    break;
  case 'redukcioni':
    cena_ona =1900;
    break;
  case 'balans':
    cena_ona = 2000;
    break;
  case 'muscle':
    cena_ona = 2100;
    break;
}

    switch (on) {
  case 0:
    cena_on = 0;
    break;
  case 'redukcioni':
    cena_on =2000;
    break;
  case 'balans':
    cena_on = 2100;
    break;
  case 'muscle':
    cena_on = 2200;
    break;
}
  
    if(cena_on>0 && cena_ona>0 && dana>0)
    {   
        
        if(cena_on > cena_ona)
        {


            cena_ona = cena_ona * dana;
            cena_on = cena_on * dana;
            snizenje = cena_ona *0.7;
            ukupno = snizenje+cena_on;
            usteda = cena_ona*0.3;

            snizenje = Math.ceil(snizenje);
            usteda = Math.ceil(usteda);
            ukupno = Math.ceil(ukupno);

            document.getElementById("polje_ona").innerHTML = "PAKET ZA NJU:  " + cena_ona + "  -30% =  " + snizenje;
            document.getElementById("polje_on").innerHTML = "PAKET ZA NJEGA:  " + cena_on;
            document.getElementById("usteda").innerHTML = "UŠTEDA:  " + usteda;
            
            document.getElementById("ukupno").innerHTML = "UKUPNO:  " + ukupno;
        }
        else
        {
            cena_ona = cena_ona * dana;
            cena_on = cena_on * dana;
            snizenje = cena_on *0.7;
            ukupno = snizenje+cena_ona;
            usteda = cena_on*0.3;

            snizenje = Math.ceil(snizenje);
            usteda = Math.ceil(usteda);
            ukupno = Math.ceil(ukupno);

            document.getElementById("polje_ona").innerHTML = "PAKET ZA NJU:  " + cena_ona;
            document.getElementById("polje_on").innerHTML = "PAKET ZA NJEGA:  " + cena_on  + "  -30% =  " + snizenje;
            document.getElementById("usteda").innerHTML = "UŠTEDA:  " + usteda;
            
            document.getElementById("ukupno").innerHTML = "UKUPNO:  " + ukupno;
        }
    }
    else
    {
        cena_ona = cena_ona * dana;
        cena_on = cena_on * dana;
        ukupno = cena_on + cena_ona;


        document.getElementById("polje_ona").innerHTML = "PAKET ZA NJU:  " + cena_ona;
        document.getElementById("polje_on").innerHTML = "PAKET ZA NJEGA:  " + cena_on;
        document.getElementById("usteda").innerHTML = "UŠTEDA:  0";
            
        document.getElementById("ukupno").innerHTML = "UKUPNO:  " + ukupno;
    }
    
}
</script>


@stop
