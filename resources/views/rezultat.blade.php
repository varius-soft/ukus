@extends('layout')

@section('title')
UKUS Kalkulator - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">UKUS KALKULATOR </h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>UKUS Kalkulator</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->


<!--
<section class="boxes" id="kako-smo-nastali">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 bordered_block grey_border">
                <div class="container text-center">
                    <h2 class="text-center">MUSCLE PAKET </h2>
                            <div class="icon"><span style="color: black;" class="ti-arrow-up"></span></div>
                            <h5 style="color: black;">MUSCLE PAKET JE SAVRŠENO REŠENJE ZA VAS!</h5>
                            <p style="color: black;">Ovaj paket je pravo rešenje za one koji žele da budu u zdravom kalorijskom suficitu i na taj način izgrade kvalitetnu mišićnu masu. U nastavku pogledajte nutritivne vrednosti ovog paketa.</p>
                        
                   
                </div>
                
            </div>
        </div>
    </div>
</section>
-->

<!-- Contacts -->
<section class="boxes" id="contacts">
    <div class="container-fluid">
        
        <div class="row">
            
            <!-- Contacts -->
            <div class="col-md-6 bordered_block image_bck white_txt" data-image="images/pozadina-crna.jpg">
                <div class="over" data-opacity="0.7" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-center">
                    <h2 class="text-center">{{$jelovnik}} PAKET <span class="ti-arrow-up"></span></h2>
                            <h5 >{{$jelovnik}} JE SAVRŠENO REŠENJE ZA VAS!</h5>
                            <p>Ovaj paket je pravo rešenje za one koji žele da budu u zdravom kalorijskom suficitu i na taj način izgrade kvalitetnu mišićnu masu. U nastavku pogledajte nutritivne vrednosti ovog paketa.</p>
                    <h3>
                        {{$kalorije}} kcal <br>
                        PROTEINI {{$proteini}} % <br>
                        MASTI {{$masti}} % <br>
                        UGLJENI HIDRATI {{$hidrati}} %

                    </h3>
                    <a class="btn btn-default" href="#poruci">poručiTE paket</a>
                </div>    
            </div>
            
            <!-- Write Us -->
           <div class="col-md-6 bordered_block image_bck grey_border" data-color="#fff">
                <div class="over" data-opacity="0.02" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                     <h2 class="text-center">VAŠI PARAMETRI: </h2>
                    <form action="/kalkulator-rezultat" name="CALC" id="CALC">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="visina" class="form-control form-opacity" placeholder="Visina (u cm)*" value="{{$visina}}">

                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="tezina" class="form-control form-opacity" placeholder="Težina (u kg)*" value="{{$tezina}}">
                            </div>
                             
                            <div class="col-md-12">
                               <select required name="pol" class="form-control">
                                    <option disabled="">Pol</option>
                                    <option @if($pol=='zenski') selected @endif value="zenski">Ženski</option>
                                    <option @if($pol=='muski') selected @endif value="muski">Muški</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="cilj" class="form-control">
                                    <option disabled="">Želim da:</option>
                                    <option @if($cilj=='mrsavljenje') selected @endif value="mrsavljenje">Izgubim višak kilograma</option>
                                    <option @if($cilj=='balans') selected @endif value="balans">Da se hranim izbalansirano</option>
                                    <option @if($cilj=='masa') selected @endif value="masa">Povećam mišićnu masu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="aktivnost" class="form-control">
                                    <option disabled="">Nivo fizičke aktivnosti:</option>
                                    <option @if($aktivnost==0) selected @endif value="0">Ne treniram uopšte</option>
                                    <option @if($aktivnost==1) selected @endif value="1">1-2 treninga nedeljno</option>
                                    <option @if($aktivnost==2) selected @endif  value="2">3-4 treninga nedeljno</option>
                                    <option @if($aktivnost==3) selected @endif value="3">5-7 treninga nedeljno</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="dan" class="form-control">
                                    <option disabled="">Koliko ste aktivni tokom dana?</option>
                                    <option @if($dan==0) selected @endif value="0">Uglavnom sedim (automobil/kancelarija)</option>
                                    <option @if($dan==1) selected @endif  value="1">Umereno sam aktivan/na</option>
                                    <option @if($dan==2) selected @endif value="2">Stalno sam u pokretu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Navedite namirnice koje ne želite da se nađu na Vašem personalizovanom jelovniku (ili ste alergični na neke od namirnica)."  class="form-control form-opacity">{{$namirnice}}</textarea>
                            </div>
                            <div class="col-md-12  text-center" >
                                <input type="submit" form="CALC" class="submit btn btn-default btn-lg active" value="PONOVO IZRAČUNAJ">
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- Write Us End -->

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- Contacts End -->

 <div class="row" id="poruci">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container simple_block text-left">
            <h2>PORUČITE PAKET</h2>
                    <form action="/poruci-paket" id="kontakt_forma" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="ime_prezime" class="form-control form-opacity" placeholder="Ime*">
                            </div>
                            
                            
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" name="mail" id="email" class="form-control form-opacity" placeholder="E-mail*">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" name="telefon" type="text" id="phone" class="form-control form-opacity" placeholder="Telefon">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="adresa" class="form-control form-opacity" placeholder="Adresa*">
                            </div>
                            
                            
                           
                            
                            <div class="col-md-8">
                               <select required name="opcija" class="form-control">
                                    
                                    <option value="1 dan">Dnevni meni (1 dan) <strong>2400 rsd</strong></option>
                                    <option selected value="5 dana">Nedeljni meni (5 dana) <strong>{{$cena_s * 5}} rsd ({{$cena_s}} po danu) </strong></option>
                                    <option  value="6 dana">Nedeljni meni (6 dana) <strong>{{$cena_s * 6}} rsd ({{$cena_s}} po danu) </strong></option>
                                    <option  value="20 dana">Mesečni meni (20 dana) <strong>{{$cena_m * 20}} rsd ({{$cena_m}} po danu) </strong></option>
                                    <option  value="20 dana">Mesečni meni (24 dana) <strong>{{$cena_m * 24}} rsd ({{$cena_m}} po danu) </strong></option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="poruka" placeholder="Napomena, stan, sprat..." id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12 text-center" >
                                <input type="submit" form="kontakt_forma" class="submit btn btn-default btn-lg active" value="PORUČITE">
                            </div>

                            <input type="text" hidden="" name="jelovnik" value="{{$jelovnik}}">
                            <input type="text" hidden="" name="namirnice" value="{{$namirnice}}">
                        </div>
                    </form>
        </div>
    </div>
</div> 

<!-- Partners -->
<section class="boxes" >
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/furn6.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                <div class="container text-center">

                <h2>UKUS U VAŠOJ FIRMI? POGLEDAJTE UKUS OFFICE</h2>
                <p>UKUS Office je koncept zdrave ishrane namenjen pravnim licima. Umesto svih obroka, Vašim zaposlenima svakog radnog dana isporučujemo jedan obrok i jednu užinu na adresu fimre.<BR><BR></p>
                <a class="btn btn-default" href="/ukus-office">DETALJNIJE</a>
                  
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--Partners End -->

@stop