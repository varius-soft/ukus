@extends('layout')

@section('title')
Blog - 
@stop

@section('meta')
<meta property="og:url"                content="http://volimukus.rs/blog" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="BLOG - Volim UKUS" />
<meta property="og:description"        content="BLOG - Volim UKUS" />
<meta property="og:image"              content="http://volimukus.rs/images/blog-og.jpg" />
@stop



@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Blog</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Blog</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->


<!-- Content -->
<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="bordered_block col-md-12 grey_border">
                
                <div class="container">
                    <div class="row">
                    
                        <!--Sidebar-->
                        <div class="col-md-12 col-xs-12">

                            <!-- ToolBar -->  
                            
                            
                            <div class="row masonry">
                                <!-- Item -->
                               
                                <!-- Item -->
                                @foreach($blog as $clanak)
                                <div class="col-sm-6 masonry-item">
                                    <a href="/clanak/{{$clanak->id}}" class="product_item text-center">
                                       <span class="product_photo bordered_wht_border"><img src="http://volimukus.rs/images/clanci/{{$clanak->slika}}" alt=""></span>
                                       <span class="product_title">{{$clanak->naslov}}</span>
                                       
                                       <span class="product_price">{{$clanak->autor}} - {{$clanak->titula}}</span>

                                    </a>
                                </div>
                                @endforeach
                               

                            </div>
                           
                        </div>
                        <!--Sidebar End-->
                        
                    </div>
                    <!--Row End-->

                </div>
            </div>
        </div> 
        <!-- Row End -->


    </div>
</div>
<!-- Content End -->


@stop