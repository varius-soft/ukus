@extends('layout')

@section('title')
PORUDŽBINA USPEŠNA - 
@stop

@section('sekcije')

<!--Intro-->
<section class="intro" >
       
        <div class="intro_item">

            <!-- Over -->
            <div class="over" data-opacity="0.4" data-color="#000"></div>
            <div class="into_back image_bck"  data-image="images/sport7.jpg"></div>
            

            <div class="inside_intro_block">
                <div class="ins_int_item white_txt bordered_wht_border text-center">
                    <div class="simple_block simple_block_sml">
                    	<div class="icon"><span class="ti-email"></span></div>
                        <h3>USPEŠNO IZVRŠILI PORUDŽBINU ŽELJENOG PAKETA</h3>
                        <h4>
                            Potrudićemo se da Vas kontaktiramo u najkraćem mogućem roku radi potvrde porudžbine.<br>Hvala.
                        </h4>
                        <a href="/" class="btn btn-white"><i class="ti-angle-left"></i> Nazad na početnu stranu</a> 
                       
                    </div>
                </div>
            </div>

        </div>
  
</section>
<!-- Intro End -->
    

@stop