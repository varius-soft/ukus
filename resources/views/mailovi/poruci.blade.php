<!DOCTYPE html >
<html>

<body>

<!-- HEADER -->
<div class="container" style="padding-top:70px; padding-bottom: 70px;">

    <table>
        <tr>
            <td></td>
            <td class="header container">

                <div class="content">
                    <table >
                        <tr>
                            <td>

                            </td>

                        </tr>
                    </table>
                </div>

            </td>
            <td></td>
        </tr>
    </table><!-- /HEADER -->


    <!-- BODY -->
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container">

                <div class="content">
                    <table>
                        <tr>
                            <td>
                            
                                <br/>
                                <h3>PORUDŽBINA <small>{{ date('d.m.Y - H:i', strtotime($vreme)) }}</small></h3>
                                <h4>{{$ime_prezime}}</h4>
                                <br>
                                <p>{{$jelovnik}} - {{$opcija}}</p>

                                <br/>


                                <hr>

        

                                <!-- address detals -->
                                <table class="columns" width="100%">
                                    <tr>
                                        <td>

                                            <!--- column 1 -->
                                            <table align="left" class="column">
                                                <tr>
                                                    <td>
                                                        <h5 class="">Podaci:</h5>
                                                        <p class="">
                                                            <strong>Ime i prezime: </strong>{{$ime_prezime}}<br/>
                                                            <strong>E-mail: </strong>{{$mail}}<br/>
                                                            <strong>Telefon: </strong>{{$telefon}}<br/>
                                                            <strong>Adresa: </strong>{{$adresa}}<br/>
                                                            <strong>Jelovnik: </strong>{{$jelovnik}} <br/>
                                                            <strong>Period: </strong>{{$opcija}} <br/>
                                                            <strong>Namirnice za izbacivanje: </strong> <br/>
                                                            {{$namirnice}} <br/><br/>
                                                            <strong>Napomena: </strong> <br/>
                                                            {{$poruka}}<br/><br/>
                                                        </p>
                                                        </p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!-- /column 1 -->




                                            <span class="clear"></span>

                                        </td>
                                    </tr>
                                </table>
                                <!-- /address details -->

                                <br/>

                                <p style="text-align:center;">
                                    <a class="btn" href="mailto:{{$mail}}">Odgovorite na poruku &raquo;</a>
                                </p>

                                <br/>


                            </td>
                        </tr>
                    </table>
                </div>

            </td>
            <td></td>
        </tr>
    </table>
    <!-- /BODY -->
</div>

</body>
</html>