@extends('layout')

@section('title')
UKUS Office - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">UKUS Office</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>UKUS Office</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->



 <div class="row">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container">
        	<h2></h2>
        	<h3>UKUS Office je namenjen pravnim licima koji žele svojim zaposlenima da obezbede zdrave i ukusne obroke i na taj način podigu njihovu produktivnost. Degustaciju UKUSa u Vašoj firmi možete zakazati putem <a href="/kontakt">kontakt forme</a>. </h3>


            <div class="row">
            	 <div class="col-md-4 col-sm-6">
            	 </div>
                <div class="col-md-4 col-sm-6">
                    <div class="pricing-table text-center">
                        <h2>OFFICE Paket</h2>
                        <span class="price">625 rsd</span>
                        <p>dnevno po zaposlenom</p>
                        <a class="btn btn-default" href="/kontakt">PORUČI</a>
                        <p>
                            <b>1 x </b>Ručak<br />
                            <b>1 x </b>Užina
                        </p>
                    </div>
                    <!--end of pricing table-->
                </div>
               
            </div>
            <p>
            	*Cena je izražena po jednom zaposlenom po danu. U cenu nije uračunat PDV.
            </p>
        </div>
    </div>
</div> 

<!-- Partners -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/hotel_m1.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                <div class="container text-center">

                <h2>Zainteresovani ste za lične ukus pakete?</h2>
                <p>Pogledajte naš cenovnik i poručite Vaš personalizovani jelovnik!<BR><BR></p>
                <a class="btn btn-default" href="/cenovnik">DETALJNIJE</a>
                  
                </div>
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--Partners End -->


@stop