@extends('layout')

@section('title')
{{$clanak->naslov}} - Blog - 
@stop


@section('meta')
<meta property="og:url"                content="http://volimukus.rs/clanak/{{$clanak->id}}" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="{{$clanak->naslov}}" />
<meta property="og:description"        content="Volim Ukus" />
<meta property="og:image"              content="http://volimukus.rs/images/clanci/{{$clanak->slika}}" />
@stop



@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">{{$clanak->naslov}}</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a><a href="/blog">Blog</a>{{$clanak->naslov}} </div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

    
<!-- Content -->
<div class="content">
    <div class="container-fluid">
        
        <div class="row">
            <div class="bordered_block col-sm-12 grey_border">
                
                <div class="container">
                    <div class="row">
                    
                        <!--Sidebar-->
                        <div class="col-md-12 col-xs-12">
                            
                            <!--Item-->
                            <div class="post-snippet">
                                <a href="#">
                                    <img alt="Post Image" src="http://volimukus.rs/images/clanci/{{$clanak->slika}}" />
                                </a>
                                <div class="post-title">
                                    <span class="label">{{$clanak->created_at->format('d-m-Y')}}</span>
                                    <a href="#">
                                        <h4 class="inline-block">{{$clanak->naslov}}</h4>
                                    </a>
                                </div>
                                <ul class="post-meta list-unstyled list-inline">
                                    @if($clanak->autor)
                                    <li>
                                        <i class="fa fa-user"></i>
                                        <span>{{$clanak->autor}}
                                        </span>
                                    </li>
                                    @endif
                                    @if($clanak->titula)
                                    <li>
                                        <i class="ti-tag"></i>
                                        <span>
                                            {{$clanak->titula}}
                                        </span>
                                    </li>
                                    @endif
                                </ul>

                                {!! $clanak->tekst !!}
                               

                            </div>
                        </div>
                        <!--Sidebar End-->
                        
                        
                    </div>
                    <!--Row End-->

                </div>
            </div>
        </div> 
        <!-- Row End -->


    </div>
</div>
<!-- Content End -->


@stop