@extends('layout')

@section('title')
DETOX - 
@stop

@section('sekcije')

<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">DETOX</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>DETOX</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<a href="#poruci">
<img src="images/novo/ukus-baner-4.jpg" style="width:100%;">
</a>




<!-- UKUS DETOX -->
<section class="boxes" id="partners">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block bordered_wht_border white_txt image_bck" data-image="images/novo/ukus-pozadina-1.jpg">

                <!-- Over -->
                <div class="over" data-opacity="0.6" data-color="#292929"></div>
                    <div class="container text-center">

                    <h1>DETOX</h1>
                    <h2>
                        7 dana potpuno izbalansirane vegeterijanske hrane većinski presne(sirove).
                    </h2>
                    <p>
                        <strong>
                            Očistite Vaš organizam od svih štetnihmaterija unetih hranom, alkoholom, duvanom i drugim štetnim namirnicama.
                        </strong>
    <BR>
                            DETOX program je idealan uvod u bilo koji od naših programa:<strong> TOPI KILOGRAME, GRADI MIŠIĆE, ŽIVI ZDRAVO!</strong>
                        
                    </p>
                        <h1>CENA NEDELJNOG PAKETA JE 12 000 RSD</h1>

                        <a class="btn btn-default" href="#poruci">PORUČI!</a>
                    </div>
                
            </div>
            <!-- Col End -->
        </div>

    </div>
</section>
<!--UKUS DETOX  End -->


 <div class="row" id="poruci">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container simple_block text-left">
            <h2>PORUČITE PAKET</h2>
                    <form action="/poruci-paket" id="kontakt_forma" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="ime_prezime" class="form-control form-opacity" placeholder="Ime*">
                            </div>
                            
                            
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" name="mail" id="email" class="form-control form-opacity" placeholder="E-mail*">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" name="telefon" type="text" id="phone" class="form-control form-opacity" placeholder="Telefon">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="adresa" class="form-control form-opacity" placeholder="Adresa*">
                            </div>
                            
                            
                           
                            
                            <div class="col-md-8">
                               <select required name="opcija" class="form-control">
                                    
                                    <option selected value="NEDELJNI PAKET">NEDELJNI PAKET <strong>12 000 rsd</strong></option>
                                   
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="poruka" placeholder="Napomena, stan, sprat..." id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Namirnice koje ne želite da budu na Vašem jelovniku" id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12 text-center" >
                                <input type="submit" form="kontakt_forma" class="submit btn btn-default btn-lg active" value="PORUČITE">
                            </div>
                            <input type="text" hidden="" name="jelovnik" value="DETOX">
                          
                        </div>
                    </form>
        </div>
    </div>
</div>
@stop

