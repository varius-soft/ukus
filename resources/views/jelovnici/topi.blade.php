@extends('layout')

@section('title')
Topi Kilograme - 
@stop

@section('sekcije')

<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Topi Kilograme</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Topi Kilograme</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<a href="#poruci">
<img src="images/novo/ukus-baner-2.jpg" style="width:100%;">
</a>


 <div class="row">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container">
            <h2></h2>
            <h3>Paket TOPI KILOGRAME je program ishrane sa kalorijskim deficitom u cilju redukcije telesne težine. Mršavite, ali ne gladujte, i uživajte u ukusu naših jela. Jelovnik je namenjen svim osobama koje žele redukovati potkožne i viscelarne masti uz očuvanje i unapređenje zdravstvenog statusa. </h3>

        </div>
    </div>
</div> 


 <div class="row" id="poruci">
    <div class="bordered_block col-sm-12 grey_border">
        <div class="container simple_block text-left">
            <h2>PORUČITE PAKET</h2>
                    <form action="/poruci-paket" id="kontakt_forma" method="POST">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="ime_prezime" class="form-control form-opacity" placeholder="Ime*">
                            </div>
                            
                            
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" name="mail" id="email" class="form-control form-opacity" placeholder="E-mail*">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" name="telefon" type="text" id="phone" class="form-control form-opacity" placeholder="Telefon">
                            </div>
                            <div class="col-md-4">
                                <input required style="color: black;" type="text" id="ime_prezime" name="adresa" class="form-control form-opacity" placeholder="Adresa*">
                            </div>
                            
                            
                           
                            
                            <div class="col-md-8">
                               <select required name="opcija" class="form-control">
                                    
                                    <option value="ŽENSKI 1 dan">ŽENSKI Dnevni meni (1 dan) <strong>2 400 rsd</strong></option>
                                    <option value="ŽENSKI 5 dana">ŽENSKI Nedeljni meni (5 dana) <strong>9 500 rsd (1 900 po danu) </strong></option>
                                    <option  value="ŽENSKI 6 dana">ŽENSKI Nedeljni meni (6 dana) <strong>11 400 rsd (1 900 po danu) </strong></option>
                                    <option  value="ŽENSKI 20 dana">ŽENSKI Mesečni meni (20 dana) <strong>32 300 rsd (1 615 po danu) </strong></option>
                                    <option  value="ŽENSKI 24 dana">ŽENSKI Mesečni meni (24 dana) <strong>38 760 rsd (1 615 po danu) </strong></option>

                                    <option value="MUŠKI 1 dan">MUŠKI Dnevni meni (1 dan) <strong>2400 rsd</strong></option>
                                    <option value="MUŠKI 5 dana">MUŠKI Nedeljni meni (5 dana) <strong>10 000 rsd (2 000 po danu) </strong></option>
                                    <option  value="MUŠKI 6 dana">MUŠKI Nedeljni meni (6 dana) <strong>12 000 rsd (2 000 po danu) </strong></option>
                                    <option  value="MUŠKI 20 dana">MUŠKI Mesečni meni (20 dana) <strong>34 000 rsd (1 700 po danu) </strong></option>
                                    <option  value="MUŠKI 24 dana">MUŠKI Mesečni meni (24 dana) <strong>40 800 rsd (1 700 po danu) </strong></option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="poruka" placeholder="Napomena, stan, sprat..." id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Namirnice koje ne želite da budu na Vašem jelovniku" id="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12 text-center" >
                                <input type="submit" form="kontakt_forma" class="submit btn btn-default btn-lg active" value="PORUČITE">
                            </div>
                            <input type="text" hidden="" name="jelovnik" value="TOPI KILOGRAME">
                          
                        </div>
                    </form>
        </div>
    </div>
</div>
@stop

