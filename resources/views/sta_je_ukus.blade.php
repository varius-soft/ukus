@extends('layout')

@section('title')
Šta je UKUS? - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Šta je UKUS?</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Šta je UKUS?</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->


<!-- Welcome -->
<section class="boxes" id="o-ukusu">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>O UKUSU</h2>
                            <h4 class="subtitle">Šta je to UKUS? </h4>
                            <p>
                            	UKUS je koncept zdravih, svežih namirnica, bez aditiva, zaslađivača i konzervanasa.
								<br>
								UKUS sadrži izbalansirane obroke u odnosu ugljenih hidrata, masti i proteina.
                            </p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <!-- Col End -->
        </div>

        <!-- Welcome Image -->
        <div class="intro_image intro_text_rb text-center z0 wow fadeInUp" data-wow-duration="2s">
            <img src="images/kesa.png" alt="">
        </div> 
    </div>
</section>
<!-- Welcome End -->



<!--Title-->
<section class="boxes simple_title text-center" id="tim">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12  white_txt bordered_wht_border image_bck" style="background-color: rgb(14, 14, 14);" data-color="#0e0e0e" data-position="top">
                <div class="simple_block white_txt">
                    <h2 class="wow fadeInUp">Zašto smo nastali?</h2>
                </div>               
            </div>
        </div>

    </div>
</section>
<!--Title End-->



<section class="boxes" id="kako-smo-nastali">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
             

                    <div class="mid_wrapper">
                        <div class="mid_box text-center">
                            <div class="icon"><span class="ti-stats-up"></span></div>
                            <h5>UZBUNA! </h5>
                            <p>56% Srba je gojazno! Najveći porast gojaznosti je kod dece do 19 godina (čak 20%) ! </p>
                        </div>
                        <div class="mid_box text-center">
                            <div class="icon"><span class="ti-info-alt"></span></div>
                            <h5>UKUS kao potreba</h5>
                            <p>UKUS je koncept zdravih, svežih namirnica, bez aditiva, zaslađivača i konzervanasa.</p>
                        </div>

                        <div class="mid_box text-center">
                            <div class="icon"><span class="ti-face-smile"></span></div>
                            <h5>UKUS kao zdravo rešenje</h5>
                            <p>UKUS je tu da pokaže da zdrava hrana lako može biti i uksna.</p>
                        </div>
                        
                    </div>
                   
                </div>
                
            </div>
        </div>
    </div>
</section>





<!--
<section class="boxes simple_title text-center" id="sirovine">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12  white_txt bordered_wht_border image_bck" style="background-color: rgb(14, 14, 14);" data-color="#0e0e0e" data-position="top">
                <div class="simple_block white_txt">
                    <h2 class="wow fadeInUp">Sirovine koje koristimo</h2>
                    <h4 class="wow fadeInUp subtitle" data-wow-delay="0.4s">Kvalitetan izbor organskih namirnica, moderno pakovanje i pravi "UKUS" su dokaz našeg kvaliteta.</h4>
                </div>               
            </div>
        </div>

    </div>
</section>

<section class="boxes" id="services">
    <div class="container-fluid">
        <div class="row">
            
            
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h2 class="wow fadeInUp"><b>Savršen balans! </b><br>Ukus sadrži savršeno izbalansirane obroke u vidu ugljenih hidrata (50%) , proteina (25%) i masti (25%).</h2>

                        </div>
                        <div class="col-md-6">
                            <h4 class="subtitle"></h4>
                            <div class="progress"> 
                              <div class="progress-bar text-left" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" data-color="#B0E0E6">
                                <span>Ugljeni hidrati 50%</span>
                              </div>
                            </div>

                            <div class="progress"> 
                              <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" data-color="#FA8072">
                                <span>Proteini 25%</span>
                              </div>
                            </div>

                            <div class="progress"> 
                              <div class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" data-color="#3CB371">
                                <span>Masti 25%</span>
                              </div>
                            </div>
                        </div>
                    </div>
           
                    
                    
                </div>
            </div>
            
        </div>

    </div>
</section>
 -->

<!--
<section class="boxes reviews">
    <div class="container-fluid">

       
        <div class="mid_wrapper">
           
            <div class="bordered_block flex_block image_bck bordered_zoom bordered_zoom_three bordered_zoom_gold height400">
                <div class="image_over image_bck" data-image="images/namirnice/meso1.jpg"></div>
                <div class="box_content">
                    <h3>Junetina</h3>
                   Najkvalitetniji komadi junetine (biftek i ramstek)
                </div>     
            </div>

         

        </div>
    
    </div>
</section>
-->

@stop