@extends('layout')

@section('title')
Beskontaktno placanje i dostava - 
@stop

@section('sekcije')
<!-- Inside Title -->
<div class="inside_title image_bck white_txt bordered_wht_border" data-color="#0e0e0e">
    <div class="container">
        <div class="row">
            <div class="col-md-6"><h1 style="color:white; ">Beskontaktna dostava i plaćanje</h1></div>
            <div class="col-md-6 text-right"><div class="breadcrumbs"><a href="/">Naslovna</a>Beskontaktna dostava i plaćanje</div></div>
        </div>       
    </div>
</div>
<!-- Inside Title End -->

<img  src="/images/beskontaktna-dostava-placanje.jpg" style="width: 100%;">  


<!-- Welcome -->
<section class="boxes" id="o-ukusu">
    <div class="container-fluid">
        <div class="row">
            
            <!-- col -->
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>UKUS BRINE O ZDRAVLJU SVOJIH KLIJENATA!</h2>
                            <h4 class="subtitle">Kako funkcioniše beskontaktna dostava? </h4>
                            <p>
                                1. Dostavljač preuzima hranu spremljenu u sigurnom, higijenskom okruženju.
                                <br>
                                2. Dostavljač dolazi na Vašu adresu i postavlja higijenski stalak na mesto dostave.
                                <br>
                                3. Dostavljač stavlja hranu spakovanu u UKUS kesu na stalak.
                                <br>
                                4. Dostavljač se udaljava na sigurnu razdaljinu (najmanje 2m) i obaveštava klijenta da je porudžbina spremna.
                                <br>
                                4. Klijent preuzima hranu sa stalka.

                            </p>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <!-- Col End -->
        </div>

        <!-- Welcome Image -->
        <div class="intro_image intro_text_rb text-center z0 wow fadeInUp" data-wow-duration="2s">
            <img src="images/kesa.png" alt="">
        </div> 
    </div>
</section>
<!-- Welcome End -->


<section class="boxes telefon" id="kako-smo-nastali">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
                    <h2 class="text-center">Šta je beskontaktno plaćanje? </h2>

                    <div class="mid_wrapper">
                        <div class="mid_box text-center">
                            <div class="icon">1</div>
                            <h5>Poručite Vaš paket </h5>
                            <p>Izaberite Vaš personalizovani jelovnik na našem sajtu.</p>
                        </div>
                        <div class="mid_box text-center">
                            <div class="icon">2</div>
                            <h5>Platite online</h5>
                            <p>Uplatnicu koju dobijete možete platiti uplatom na račun ili putem e-bankinga online.</p>
                        </div>

                        <div class="mid_box text-center">
                            <div class="icon">3</div>
                            <h5>Potvrda porudžbine</h5>
                            <p>Pošaljite nam potvrdu o uplati ili sačekajte da Vas kontaktiramo radi dogovora o preuzimanju pošiljki.</p>
                        </div>
                        
                    </div>
                   
                </div>
                
            </div>
        </div>
    </div>
</section>

<section class="boxes komp" id="kako-smo-nastali">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 bordered_block grey_border">
                <div class="container">
                    <h2 class="text-center">Šta je beskontaktno plaćanje? </h2>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <div class="icon">1</div>
                            <h5>Poručite Vaš paket </h5>
                            <p>Izaberite Vaš personalizovani jelovnik na našem sajtu.</p>
                        </div>
                        <div class="col-sm-12 text-center">
                            <div class="icon">2</div>
                            <h5>Platite online</h5>
                            <p>Uplatnicu koju dobijete možete platiti uplatom na račun ili putem e-bankinga online.</p>
                        </div>

                        <div class="col-sm-12 text-center">
                            <div class="icon">3</div>
                            <h5>Potvrda porudžbine</h5>
                            <p>Pošaljite nam potvrdu o uplati ili sačekajte da Vas kontaktiramo radi dogovora o preuzimanju pošiljki.</p>
                        </div>
                        
                    
                   
                </div>
                
            </div>
        </div>
    </div>
</section>


<!-- Contacts -->
<section class="boxes" id="contacts">
    <div class="container-fluid">
        
        <div class="row">
            
            <!-- Contacts -->
            <div class="col-md-6 bordered_block image_bck white_txt" data-image="images/pozadina-crna.jpg">
                <div class="over" data-opacity="0.7" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <h2>Poručite Vaš personalizovani jelovnik!</h2>
                    <p>
                        Unesite potrebne parametre i mi ćemo za Vas napraviti personalizovani jelovnik koji savršeno odgovara Vašim svakodnevnim aktivnostima, načinu života, kao i cilju koji želite da postignete.
                    </p>
                    
                    <img style="width: : 80%" src="images/badem-paradajz-za-belu-pozadinu.png">
                    
                </div>    
            </div>
            
            <!-- Write Us -->
           <div class="col-md-6 bordered_block image_bck grey_border" data-color="#fff">
                <div class="over" data-opacity="0.02" data-color="#121d2a"></div>
                <div class="col-md-12 simple_block text-left">
                    <form action="/kalkulator-rezultat" name="CALC" id="CALC">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="visina" class="form-control form-opacity" placeholder="Visina (u cm)*">
                            </div>

                            <div class="col-md-12">
                                <input required style="color: black;" type="text" id="ime_prezime" name="tezina" class="form-control form-opacity" placeholder="Težina (u kg)*">
                            </div>
                             
                            <div class="col-md-12">
                               <select required name="pol" class="form-control">
                                    <option selected disabled="">Pol</option>
                                    <option value="zenski">Ženski</option>
                                    <option value="muski">Muški</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="cilj" class="form-control">
                                    <option selected disabled="">Želim da:</option>
                                    <option value="mrsavljenje">Izgubim višak kilograma</option>
                                    <option value="balans">Da se hranim izbalansirano</option>
                                    <option value="masa">Povećam mišićnu masu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="aktivnost" class="form-control">
                                    <option selected disabled="">Nivo fizičke aktivnosti:</option>
                                    <option value="0">Ne treniram uopšte</option>
                                    <option value="1">1-2 treninga nedeljno</option>
                                    <option value="2">3-4 treninga nedeljno</option>
                                    <option value="3">5-7 treninga nedeljno</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                               <select required name="dan" class="form-control">
                                    <option selected disabled="">Koliko ste aktivni tokom dana?</option>
                                    <option value="0">Uglavnom sedim (automobil/kancelarija)</option>
                                    <option value="1">Umereno sam aktivan/na</option>
                                    <option value="2">Stalno sam u pokretu</option>
                                </select>
                            </div>

                            <div class="col-md-12">
                                <textarea style="color: black;" name="namirnice" placeholder="Navedite namirnice koje ne želite da se nađu na Vašem personalizovanom jelovniku (ili ste alergični na neke od namirnica)."  class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12  text-center">
                                <input type="submit" form="CALC" class="submit btn btn-default btn-lg active" value="Prikaži rezultat">
                            </div>
                        </div>
                    </form>
                </div>   
            </div>
            <!-- Write Us End -->

        </div>
        <!-- Row End -->
    </div>
</section>
<!-- Contacts End -->

@stop



